require 'singleton'
class DataContainer
	begin
		include Singleton
		@data_user_obj
		@input_user_obj
		def get_data_user_obj
			return @data_user_obj
		end
		def set_data_user_obj(data_user_obj)
			@data_user_obj=data_user_obj
		end
		def get_input_user_obj()
			return @input_user_obj
		end
		def set_input_user_obj(input_obj_array)
			@input_user_obj=input_obj_array
		end	
	rescue Exception => e
 	 raise e
  end
end