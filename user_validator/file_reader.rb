require_relative 'user_creator'
class FileReader
require 'csv'
	begin
		def read_file(type_of_file,file_path)
			if validate_file(file_path)
				file_data_array = CSV.read(file_path)
				user_creator_obj=UserCreator.new
					if type_of_file.eql?("data")
						user_creator_obj.create_data_users(file_data_array)
					else
						user_creator_obj.create_input_users(file_data_array)
					end
			end
		end

		def validate_file(file_path)
			return check_file_permissions(file_path)&&file_exist(file_path)
		end

		def check_file_permissions(file_path)
      return File.readable?(file_path)
    end

    def file_exist(file_path)
     return File.exist?(file_path)
    end
    
		rescue Exception => e
    raise e
   end
end