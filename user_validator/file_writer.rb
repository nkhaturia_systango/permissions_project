require_relative 'data_container'
class FileWriter
	require 'csv'
	begin
		def write_file(file_path)
			if validate_file(file_path)
				data_container_obj=DataContainer.instance
				input_obj_array=data_container_obj.get_input_user_obj
				CSV.open(file_path, "w+") do |csv|
					csv <<["USER NAME","PASSWORD","PERMISSION REQUISTED","MESSAGE"]
					for i in 0..input_obj_array.length-1
						csv << [input_obj_array[i].get_user_name,input_obj_array[i].get_password,input_obj_array[i].get_permissions,input_obj_array[i].get_user_validity]
       		end
   			end
			end			
		end
		def validate_file(file_path)
			return check_file_permissions(file_path)&&file_exist(file_path)
		end
		def check_file_permissions(file_path)
   		return File.writable?(file_path)
   	end
   	def file_exist(file_path)
   		return File.exist?(file_path)
   	end
		rescue Exception => e
  	raise e
  end
end
