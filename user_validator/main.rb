require_relative 'file_reader'
require_relative 'validate_user'
require_relative 'file_writer'
data_file_path="data.csv"
input_file_path="input.csv"
output_file_path="output.csv"
file_writer_obj=FileWriter.new
file_reader_obj =FileReader.new
validate_user_obj=ValidateUser.new
begin 
	file_reader_obj.read_file("data",data_file_path)																
	file_reader_obj.read_file("input",input_file_path)															
	validate_user_obj.validate_user()
	file_writer_obj.write_file(output_file_path)
rescue Exception => e
	puts e.message
end