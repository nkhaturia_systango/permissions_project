class User
begin
	@user_name
	@password
	@permissions
	@user_validity
	def get_user_name
		return @user_name
	end
	def get_permissions
		return @permissions
	end	
	def set_user_name(user_name)
	 	@user_name=user_name
	end
	
	def set_permissions(permissions)
		@permissions= permissions	
	end 
	def get_user_validity()
		return @user_validity
	end
	def set_user_validity(validity)
		@user_validity=validity
	end
	def set_password(password)
		@password=password
	end
	def get_password()
		return @password
	end
rescue Exception => e
 	 raise e
  end
end