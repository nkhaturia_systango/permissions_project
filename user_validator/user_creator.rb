require_relative 'user'
require_relative 'data_container'
class UserCreator
begin
	def create_data_users(data_file_array)
		data_user_obj=[]
		for i in 0..data_file_array.length-1
			data_user_obj[i]=User.new
		  data_user_obj[i].set_user_name(data_file_array[i][0])
		  data_user_obj[i].set_password(data_file_array[i][1])
		  data_user_obj[i].set_permissions(data_file_array[i][2])
		end
			data_container_obj=DataContainer.instance
			data_container_obj.set_data_user_obj(data_user_obj)
	end
	def create_input_users(input_file_array)
		input_user_obj=[]
		for i in 0..input_file_array.length-1
			input_user_obj[i]=User.new
			input_user_obj[i].set_user_name(input_file_array[i][0])
			input_user_obj[i].set_password(input_file_array[i][1])
			input_user_obj[i].set_permissions(input_file_array[i][2])
		end
		data_container_obj=DataContainer.instance
		data_container_obj.set_input_user_obj(input_user_obj)
	end
rescue Exception => e
    raise e
   end
end