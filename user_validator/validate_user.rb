require_relative 'data_container'
class ValidateUser
begin 
	def check_subset(data_user_permission,input_user_permission)
		require 'set'
		data_array=data_user_permission.split('~')
		input_array=input_user_permission.split('~')
		data_array_set=data_array.to_set
		input_array_set=input_array.to_set
		return input_array_set.subset? data_array_set
	end
	def validate_user()
		data_container_obj=DataContainer.instance
		data_file_users=data_container_obj.get_data_user_obj
		input_file_users=data_container_obj.get_input_user_obj	
		data_file_hash = create_hash(data_file_users)
		input_file_users.each do |input_user|
			data_user = data_file_hash[input_user.get_user_name]
			if(data_user.nil?)
				input_user.set_user_validity("USER NOT FOUND")
				next
			end
			if(!input_user.get_password.eql?(data_user.get_password))
				input_user.set_user_validity("PASSWORD INCORRECT")
			next
			end
			result = check_subset(data_user.get_permissions, input_user.get_permissions)
			if(!result)
				input_user.set_user_validity("PERMISSION DENIED ")
			next
			end
			input_user.set_user_validity("VALID USER")
			end
			set_input_to_data_container(input_file_users)
	end
	def set_input_to_data_container(input_file_users)
		data_container_obj=DataContainer.instance
		data_container_obj.set_input_user_obj(input_file_users)
	end
	def create_hash(data_file_users)
		data_file_hash = Hash.new
		data_file_users.each do |data_user|
			data_file_hash[data_user.get_user_name] = data_user
		end
		return data_file_hash
	end
rescue Exception => e
 	 raise e
  end
end
